from Gaudi.Configuration import *
import os
from SR1.common_io import *
from SR1.common_lzap_modules import *
from SR1.catalog_modules import *

inputFiles = getInputFiles()
sparseInputFile = os.getenv('LZAP_SPARSE_INPUT')

try:
    outputFile = os.environ['LZAP_OUTPUT_FILE']
except KeyError:
    outputFile = "lzap.root"

nEvents = os.getenv('LZAP_NEVENTS')
if nEvents is None:
    nEvents = -1

app = ApplicationMgr()
app.EvtMax = nEvents
app.HistogramPersistency = "NONE"
app.EvtSel = "Ldrf"
app.OutputLevel = 3

# turn on auditors
app.Dlls += { "GaudiAud" };

# add timing auditor
AuditorSvc().Auditors += { "ChronoAuditor" }


#################################################
# Configure inputs, outputs
selector    = configure_LDRF_inputs(app, inputFiles)
if sparseInputFile is not None:
    # if selector.SparseEventFile is a non-empty string, LZap ignores all
    # other inputs and runs in sparse processing mode
    selector.SparseEventsFile = sparseInputFile
condSvc     = configure_conditions_svc(app)
condSvc.ChannelResponseFile='/cvmfs/lz.opensciencegrid.org/LZap/ConditionsDataModel/SPEandDPEcalibrations-11Nov21.txt'
outStream   = configure_RQ_output(app, outputFile, pdm_to_rq_addresses())
app.OutStream += [ outStream ]
configure_dqm_output(app, configure_dqm_filename(outputFile))


#################################################
# Define processing chain
# Order is important.
modules = OrderedDict()
modules.update(common_modules())
modules.update(tpc_modules())
modules.update(skin_modules())
modules.update(od_modules())
modules.update(interaction_modules())
modules.update(ext_channel_modules())
modules.update(dqm_modules())

#################################################
# Configure processing chain for production pipeline
if os.getenv("LZAP_PROD_MODE") == '1':
    # Enable creation of .db entries for RQ files
    modules.update(catalog_modules_rq(selector.InputFiles, outStream.OutputFile))

if os.getenv("LZAP_PROMPT_PROC") == '1':
    # Enable creation of .db "update" entries for input raw files
    from DaqLdrfModules import DaqLdrfModulesConf
    dbOutDir = os.path.dirname(outStream.OutputFile)
    if "" == dbOutDir:
        dbOutDir = "."
    modules.update(catalog_modules_raw(dbOutDir, LZapProcessed=True))

# Turn on auditors for all modules
for module in modules:
    modules[module].AuditExecute = True


# module parameter customizations
set_standard_module_parameters(modules)

app.TopAlg = prepare_chain_for_lzap(modules)
