#
# Run ganga jobs
#
# Running commands:
# hdfs dfs -mkdir -p ${base_location}/${particle}/lzap_output
# hdfs dfs -mkdir -p ${base_location}/${particle}/baccarat_verbose
# conda activate ganga
# ganga batch_submission.py
#

particle = 'amli_CSD1_700mm'


base_save_location = '/user/ak18773/Water_Foam/AcrylicG4Box/' + particle + '/'

n_jobs = 100
start_seed = 8000
splitter_env_vals = []
for i in range(start_seed, start_seed + n_jobs):
    variable_dict = {'SEED': str(i),
                     'PARTICLE': particle,
                     'OUTPUT_FILENAME': particle + '_',
                     'MACRO': particle + '.mac',
                     'HDFS_OUTPUT_DIR': base_save_location,
                     'NBEAMON': '10000',
                     'PYTHON_SCRIPT': 'baccarat_verbose_reader.py',
                     'RUN_VERBOSE_ANALYSIS': "0"
                     }

    splitter_env_vals.append(variable_dict)


    
j = Job()
j.application = Executable()
j.application.exe = File('worker_node_script_edep.sh')
j.splitter = GenericSplitter()
j.splitter.attribute = 'application.env'
j.splitter.values = splitter_env_vals
j.inputfiles = [LocalFile('edep_macros/{0}.mac'.format(particle))]
j.backend = Local()
j.submit()

