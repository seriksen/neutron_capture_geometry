#########################################################
# Title: gdls_neutrons.mac
# Author: Sam Eriksen
# BACCARAT: release 6.2.11
# Sources: low energy neutrons spawning in the GdLS
#########################################################
/run/verbose       0
/control/verbose   0
/tracking/verbose  1

#########################################################
# IO
#########################################################
/Bacc/io/enableRootOutput 0
/Bacc/io/enableBinaryOutput 1

/control/getEnv OUTPUT_DIR
/Bacc/io/outputDir {OUTPUT_DIR}

/control/getEnv OUTPUT_FILENAME
/Bacc/io/outputName {OUTPUT_FILENAME}

/Bacc/io/alwaysRecordPrimary true

/Bacc/io/updateFrequency  1

#########################################################
# Set Record Levels
#########################################################
/Bacc/detector/recordLevel LiquidXenonTarget 5
/Bacc/detector/recordLevel BottomGridHolder 5
/Bacc/detector/recordLevel ReverseFieldRegion 5
/Bacc/detector/recordLevel CathodeGridHolder 5
/Bacc/detector/recordLevel GateGridHolder 5
/Bacc/detector/recordLevel LiquidSkinXenon 5
/Bacc/detector/recordLevel LiquidSkinXenonBank 5
/Bacc/detector/recordLevel ScintillatorCenter 5
/Bacc/detector/recordLevel InnerGaseousXenon 5
/Bacc/detector/recordLevel AnodeGridHolder 5
/Bacc/detector/recordLevel TopGridHolder 5
/Bacc/detector/recordLevel GaseousSkinXenon 5
/Bacc/detector/recordLevel GaseousSkinXenonBank 5
/Bacc/detector/recordLevel WaterAndPMTs 5

#########################################################
# Physics list
#########################################################
/Bacc/physicsList/useOptics true
/Bacc/physicsList/maxCereEleSteps 1000

/Bacc/physicsList/genTimesToZero true

/Bacc/physicsList/usePhotonEvaporation true

/run/initialize

/grdm/verbose      0
#########################################################
# Set the seed
#########################################################
/control/getEnv SEED
/Bacc/randomSeed {SEED}

#########################################################
# Detector setup
#########################################################
/Bacc/detector/select LZDetector

/LZ/gridWires off

/Bacc/detector/update

/Bacc/detector/useMCTruthLevels 1
#########################################################
# Material commands
#########################################################
/Bacc/materials/LXeTeflonRefl          0.973
/Bacc/materials/LXeAbsorption          100 m
/Bacc/materials/LXeSteelRefl           0.2
/Bacc/materials/GXeTeflonRefl          0.85
/Bacc/materials/GXeBlackWallRefl       0.2
/Bacc/materials/GXeSteelRefl           0.2
/Bacc/materials/GXeAbsorption          500 m
/Bacc/materials/AlUnoxidizedQuartzRefl 0.9
/Bacc/materials/GXeRayleigh            500 m
/Bacc/materials/LXeRayleigh            0.3 m
/Bacc/materials/QuartzAbsorption       1000 km
/Bacc/materials/LXeTeflonReflLiner    0.98
/Bacc/materials/LXeTeflonReflPMT      0.95
/Bacc/materials/LXeTeflonReflPlate    0.9
/Bacc/materials/LXeTeflonReflCables   0.1
/Bacc/materials/LXeTeflonReflTruss    0.8
/Bacc/materials/LXeTeflonReflBskinPMT 0.8
/Bacc/materials/LXeTeflonReflBplate   0.9
/Bacc/materials/LXeTeflonReflTskinPMT 0.8
/Bacc/materials/LXeTeflonReflWeir     0.1
/Bacc/materials/LXeTeflonReflDomePMT  0.8

#########################################################
# Set the Record Levels: Old BIN Format
#########################################################
/Bacc/detector/useMapOptical true

#########################################################
# Masses
#########################################################
/Bacc/detector/setComponentMass LiquidXenonTarget 7000 kg
# Xe Masses
/Bacc/detector/setComponentMass LiquidSkinXenon 1951.333 kg
/Bacc/detector/setComponentMass GaseousSkinXenon 1.95 kg
/Bacc/detector/setComponentMass GaseousSkinXenonBank 0.65 kg
/Bacc/detector/setComponentMass LiquidSkinXenonBank 650.419 kg
/Bacc/detector/setComponentMass LiquidXenonTarget 7000.410 kg
/Bacc/detector/setComponentMass InnerGaseousXenon 2.80 kg
/Bacc/detector/setComponentMass ReverseFieldRegion 659.457 kg
/Bacc/detector/setComponentMass GateGridHolder 0.736 kg
/Bacc/detector/setComponentMass CathodeGridHolder 0.976 kg
/Bacc/detector/setComponentMass BottomGridHolder 0.736 kg
/Bacc/detector/setComponentMass AnodeGridHolder 5.07E-03 kg
/Bacc/detector/setComponentMass ScintillatorCenter 17300 kg
# PMT Masses
/Bacc/detector/setComponentMass Top_PMT_Ceramic 1.570E-02 kg
/Bacc/detector/setComponentMass Bottom_PMT_Ceramic 1.570E-02 kg
/Bacc/detector/setComponentMass Top_PMT_Body 1.317E-01 kg
/Bacc/detector/setComponentMass Bottom_PMT_Body 1.317E-01 kg
/Bacc/detector/setComponentMass Top_PMT_Base 5.661E-03 kg
/Bacc/detector/setComponentMass Bottom_PMT_Base 5.661E-03 kg
/Bacc/detector/setComponentMass Dome_Skin_PMT_Body 1.267E-01 kg
/Bacc/detector/setComponentMass Bottom_Skin_PMT_Body 1.267E-01 kg
/Bacc/detector/setComponentMass Top_Skin_PMT_Body 2.397E-02 kg
/Bacc/detector/setComponentMass Water_PMT_Window 1.02 kg
/Bacc/detector/setComponentMass Water_PMT_Can 0.686 kg
# PTFE Masses
/Bacc/detector/setComponentMass PTFEWallsInLiquid 184.0 kg
/Bacc/detector/setComponentMass PTFELinerLiquid 26.224 kg #Skin teflon outer liner
/Bacc/detector/setComponentMass TopPTFELiner 5.0278 kg #Upper PMT Structure -> Trifoils, PMT sleeves etc. for top array
/Bacc/detector/setComponentMass BottomPTFELiner 1.3527 kg #Lower PMT Structure -> Trifoils  for bottom array
/Bacc/detector/setComponentMass BottomXeSkinPTFELiner 66.7273 kg #Lower PMT Structure -> Dome skin PODs etc. pointing into skin dome
/Bacc/detector/setComponentMass TopSkinIceCubeTray 10.565 kg #This is *not* in the backgrounds table. Going to assume the same specs as TopPTFELiner
# Cryostat Masses
/Bacc/detector/setComponentMass InnerTitaniumVessel 800 kg
/Bacc/detector/setComponentMass OuterTitaniumVessel 1022 kg
/Bacc/detector/setComponentMass TitaniumLegs 454 kg
/Bacc/detector/setComponentMass FoamInsulation 13.781 kg
/Bacc/detector/setComponentMass AluminumSealantInInnerVesselFlange 7.37E-01 kg #(Al+Iconel+Niconic) The following are guesses,
/Bacc/detector/setComponentMass ElastomerSealantInInnerVesselFlange 2.29E-01 kg  #using the previous ratio of masses to divide
/Bacc/detector/setComponentMass ElastomerSealantInOuterVesselUpperFlange 5.74E-01 kg #up mass from backgrounds table
/Bacc/detector/setComponentMass ElastomerSealantInOuterVesselBottomFlange 5.74E-01 kg
/Bacc/detector/setComponentMass BoltsInInnerVesselFlange 23 kg #Bolts take on all 53.44 kg of Stainless Steel...
/Bacc/detector/setComponentMass BoltsInOuterVesselUpperFlange 15.22 kg
/Bacc/detector/setComponentMass BoltsInOuterVesselBottomFlange 15.22 kg
# Truss Masses - these absorb everything for lower/upper PMT structure, including PTFE mass that is separately used for PTFEAlphaN
/Bacc/detector/setComponentMass BottomTruss 53.625 kg
/Bacc/detector/setComponentMass TopTruss 115.143 kg
# Conduit Mass - using this to represent the entire Xe recirculation tubing
/Bacc/detector/setComponentMass BottomConduit 14.09 kg
# Outer Detector Masses
/Bacc/detector/setComponentMass ScintillatorTank 3164 kg #Acrylic + Displacement Foam
/Bacc/detector/setComponentMass SteelLegs 1100 kg
# Field Ring Masses
/Bacc/detector/setComponentMass ForwardFieldRing 1.3464 kg
/Bacc/detector/setComponentMass ReverseFieldRing 1.35 kg
# Grid Holder Masses
/Bacc/detector/setComponentMass AnodePhysGrid 19.94 kg
/Bacc/detector/setComponentMass BottomPhysGrid 11.5 kg
/Bacc/detector/setComponentMass CathodePhysGrid 17.9 kg
/Bacc/detector/setComponentMass GatePhysGrid 33.35 kg
# HV Conduit Masses
/Bacc/detector/setComponentMass HVConduitVacuumInnerSteelCone 7.70 kg  #Going again by the ratio of inner to outer cones in the TDR macro
/Bacc/detector/setComponentMass HVConduitInnerOuterSteelCone 121.30 kg  #Going again by the ratio of inner to outer cones in the TDR macro
/Bacc/detector/setComponentMass HVConduitInnerConeInnerStructure 14.87 kg
/Bacc/detector/setComponentMass HVHolePTFELinerLiquid 0.905 kg

#########################################################
# Sources
#########################################################
################  Other Alpha,N ~4156 neutrons (147 w/o OD) in 6 months  #######
/Bacc/source/set Top_PMT_Ceramic Spectrum_neutron 5.99E-00 mBq/kg /cvmfs/lz.opensciencegrid.org/BACCARAT/SpectrumGenerator/Neutrons-18Jun19/Al2O3_U_an_early_neutron.dat #~8 neutrons in 6 months
/Bacc/source/set Top_PMT_Ceramic Spectrum_neutron 1.20E+01 mBq/kg /cvmfs/lz.opensciencegrid.org/BACCARAT/SpectrumGenerator/Neutrons-18Jun19/Al2O3_U_an_late_neutron.dat #~15 neutrons in 6 months
/Bacc/source/set Top_PMT_Ceramic Spectrum_neutron 9.72E-00 mBq/kg /cvmfs/lz.opensciencegrid.org/BACCARAT/SpectrumGenerator/Neutrons-18Jun19/Al2O3_Th_an_neutron.dat #~12 neutrons in 6 months
/Bacc/source/set Bottom_PMT_Ceramic Spectrum_neutron 5.99E-00 mBq/kg /cvmfs/lz.opensciencegrid.org/BACCARAT/SpectrumGenerator/Neutrons-18Jun19/Al2O3_U_an_early_neutron.dat
/Bacc/source/set Bottom_PMT_Ceramic Spectrum_neutron 1.20E+01 mBq/kg /cvmfs/lz.opensciencegrid.org/BACCARAT/SpectrumGenerator/Neutrons-18Jun19/Al2O3_U_an_late_neutron.dat
/Bacc/source/set Bottom_PMT_Ceramic Spectrum_neutron 9.72E-00 mBq/kg /cvmfs/lz.opensciencegrid.org/BACCARAT/SpectrumGenerator/Neutrons-18Jun19/Al2O3_Th_an_neutron.dat
/Bacc/source/set Top_Skin_PMT_Body Spectrum_neutron 1.15E+01 mBq/kg /cvmfs/lz.opensciencegrid.org/BACCARAT/SpectrumGenerator/Neutrons-18Jun19/SiO2_U_an_early_neutron.dat #~4 neutrons in 6 months
/Bacc/source/set Top_Skin_PMT_Body Spectrum_neutron 3.93E+01 mBq/kg /cvmfs/lz.opensciencegrid.org/BACCARAT/SpectrumGenerator/Neutrons-18Jun19/SiO2_U_an_late_neutron.dat #~14 neutrons in 6 months
/Bacc/source/set Top_Skin_PMT_Body Spectrum_neutron 1.88E+01 mBq/kg /cvmfs/lz.opensciencegrid.org/BACCARAT/SpectrumGenerator/Neutrons-18Jun19/SiO2_Th_an_neutron.dat #~7 neutrons in 6 months
/Bacc/source/set Bottom_Skin_PMT_Body Spectrum_neutron 1.84E+01 mBq/kg /cvmfs/lz.opensciencegrid.org/BACCARAT/SpectrumGenerator/Neutrons-18Jun19/Al2O3_U_an_late_neutron.dat #~18 neutrons in 6 months
/Bacc/source/set Bottom_Skin_PMT_Body Spectrum_neutron 4.64E-00 mBq/kg /cvmfs/lz.opensciencegrid.org/BACCARAT/SpectrumGenerator/Neutrons-18Jun19/Al2O3_Th_an_neutron.dat #~5 neutrons in 6 months
/Bacc/source/set Top_PMT_Base Spectrum_neutron 1.41E+01 mBq/kg /cvmfs/lz.opensciencegrid.org/BACCARAT/SpectrumGenerator/Neutrons-18Jun19/Al2O3_U_an_late_neutron.dat #~6 neutrons in 6 months
/Bacc/source/set Bottom_PMT_Base Spectrum_neutron 1.41E+01 mBq/kg /cvmfs/lz.opensciencegrid.org/BACCARAT/SpectrumGenerator/Neutrons-18Jun19/Al2O3_U_an_late_neutron.dat
/Bacc/source/set PTFEWallsInLiquid Spectrum_neutron 3.60E-01 mBq/kg /cvmfs/lz.opensciencegrid.org/BACCARAT/SpectrumGenerator/Spectra-31May19/NeutronSpectrum_PTFE.dat #I think this one is probably for Th? Going to cheat and add in U_late to this too -> #~10 neutrons in 6 months
/Bacc/source/set ForwardFieldRing Spectrum_neutron 5.08E-01 mBq/kg /cvmfs/lz.opensciencegrid.org/BACCARAT/SpectrumGenerator/Neutrons-18Jun19/Al2O3_U_an_late_neutron.dat #~7 neutrons in 6 months
/Bacc/source/set ForwardFieldRing Spectrum_neutron 1.29E-00 mBq/kg /cvmfs/lz.opensciencegrid.org/BACCARAT/SpectrumGenerator/Neutrons-18Jun19/Al2O3_Th_an_neutron.dat #~19 neutrons in 6 months
/Bacc/source/set ReverseFieldRing Spectrum_neutron 5.08E-01 mBq/kg /cvmfs/lz.opensciencegrid.org/BACCARAT/SpectrumGenerator/Neutrons-18Jun19/Al2O3_U_an_late_neutron.dat
/Bacc/source/set ReverseFieldRing Spectrum_neutron 1.29E-00 mBq/kg /cvmfs/lz.opensciencegrid.org/BACCARAT/SpectrumGenerator/Neutrons-18Jun19/Al2O3_Th_an_neutron.dat
/Bacc/source/set InnerTitaniumVessel Spectrum_neutron 7.98E-03 mBq/kg /cvmfs/lz.opensciencegrid.org/BACCARAT/SpectrumGenerator/Neutrons-18Jun19/Titanium_U_an_late_neutron.dat #~3 neutrons in 6 months
/Bacc/source/set InnerTitaniumVessel Spectrum_neutron 3.45E-02 mBq/kg /cvmfs/lz.opensciencegrid.org/BACCARAT/SpectrumGenerator/Neutrons-18Jun19/Titanium_Th_an_neutron.dat #~14 neutrons in 6 months
/Bacc/source/set OuterTitaniumVessel Spectrum_neutron 7.98E-03 mBq/kg /cvmfs/lz.opensciencegrid.org/BACCARAT/SpectrumGenerator/Neutrons-18Jun19/Titanium_U_an_late_neutron.dat
/Bacc/source/set OuterTitaniumVessel Spectrum_neutron 3.45E-02 mBq/kg /cvmfs/lz.opensciencegrid.org/BACCARAT/SpectrumGenerator/Neutrons-18Jun19/Titanium_Th_an_neutron.dat
/Bacc/source/set TitaniumLegs Spectrum_neutron 7.98E-03 mBq/kg /cvmfs/lz.opensciencegrid.org/BACCARAT/SpectrumGenerator/Neutrons-18Jun19/Titanium_U_an_late_neutron.dat
/Bacc/source/set TitaniumLegs Spectrum_neutron 3.45E-02 mBq/kg /cvmfs/lz.opensciencegrid.org/BACCARAT/SpectrumGenerator/Neutrons-18Jun19/Titanium_Th_an_neutron.dat
/Bacc/source/set BoltsInInnerVesselFlange Spectrum_neutron 5.73E-01 mBq/kg /cvmfs/lz.opensciencegrid.org/BACCARAT/SpectrumGenerator/Neutrons-18Jun19/SS316l_Th_an_neutron.dat #~5 neutrons in 6 months
/Bacc/source/set BoltsInOuterVesselUpperFlange Spectrum_neutron 5.73E-01 mBq/kg /cvmfs/lz.opensciencegrid.org/BACCARAT/SpectrumGenerator/Neutrons-18Jun19/SS316l_Th_an_neutron.dat
/Bacc/source/set BoltsInOuterVesselBottomFlange Spectrum_neutron 5.73E-01 mBq/kg /cvmfs/lz.opensciencegrid.org/BACCARAT/SpectrumGenerator/Neutrons-18Jun19/SS316l_Th_an_neutron.dat
/Bacc/source/set ScintillatorTank Spectrum_neutron 1.62E-02 mBq/kg /cvmfs/lz.opensciencegrid.org/BACCARAT/SpectrumGenerator/Neutrons-18Jun19/polyethylene_U_an_early_neutron.dat #~8 neutrons in 6 months
/Bacc/source/set ScintillatorTank Spectrum_neutron 8.08E-03 mBq/kg /cvmfs/lz.opensciencegrid.org/BACCARAT/SpectrumGenerator/Neutrons-18Jun19/polyethylene_U_an_late_neutron.dat #~4 neutrons in 6 months
/Bacc/source/set ScintillatorTank Spectrum_neutron 1.23E-02 mBq/kg /cvmfs/lz.opensciencegrid.org/BACCARAT/SpectrumGenerator/Neutrons-18Jun19/polyethylene_Th_an_neutron.dat #~6 neutrons in 6 months
/Bacc/source/set SteelLegs Spectrum_neutron 1.79E-02 mBq/kg /cvmfs/lz.opensciencegrid.org/BACCARAT/SpectrumGenerator/Neutrons-18Jun19/SS316l_U_an_early_neutron.dat #~3 neutrons in 6 months
/Bacc/source/set SteelLegs Spectrum_neutron 4.70E-02 mBq/kg /cvmfs/lz.opensciencegrid.org/BACCARAT/SpectrumGenerator/Neutrons-18Jun19/SS316l_U_an_late_neutron.dat #~8 neutrons in 6 months
/Bacc/source/set SteelLegs Spectrum_neutron 2.89E-01 mBq/kg /cvmfs/lz.opensciencegrid.org/BACCARAT/SpectrumGenerator/Neutrons-18Jun19/SS316l_Th_an_neutron.dat #~50 neutrons in 6 months
/Bacc/source/set Water_PMT_Window Spectrum_neutron 6.45E-00 mBq/kg /cvmfs/lz.opensciencegrid.org/BACCARAT/SpectrumGenerator/Neutrons-18Jun19/SiO2_U_an_early_neutron.dat #~125 neutrons in 6 months
/Bacc/source/set Water_PMT_Window Spectrum_neutron 4.46E+01 mBq/kg /cvmfs/lz.opensciencegrid.org/BACCARAT/SpectrumGenerator/Neutrons-18Jun19/SiO2_U_an_late_neutron.dat #~862 neutrons in 6 months
/Bacc/source/set Water_PMT_Window Spectrum_neutron 3.46E+01 mBq/kg /cvmfs/lz.opensciencegrid.org/BACCARAT/SpectrumGenerator/Neutrons-18Jun19/SiO2_Th_an_neutron.dat #~667 neutrons in 6 months
/Bacc/source/set Water_PMT_Can Spectrum_neutron 1.39E-00 mBq/kg /cvmfs/lz.opensciencegrid.org/BACCARAT/SpectrumGenerator/Neutrons-18Jun19/polyethylene_U_an_early_neutron.dat #~18 neutrons in 6 months
/Bacc/source/set Water_PMT_Can Spectrum_neutron 7.27E-00 mBq/kg /cvmfs/lz.opensciencegrid.org/BACCARAT/SpectrumGenerator/Neutrons-18Jun19/polyethylene_U_an_late_neutron.dat #~94 neutrons in 6 months
/Bacc/source/set Water_PMT_Can Spectrum_neutron 3.28E-00 mBq/kg /cvmfs/lz.opensciencegrid.org/BACCARAT/SpectrumGenerator/Neutrons-18Jun19/polyethylene_Th_an_neutron.dat #~43 neutrons in 6 months
/Bacc/source/set Water_PMT_Can Spectrum_neutron 1.32E+01 mBq/kg /cvmfs/lz.opensciencegrid.org/BACCARAT/SpectrumGenerator/Neutrons-18Jun19/PVDF_U_an_early_neutron.dat #~203 neutrons in 6 months
/Bacc/source/set Water_PMT_Can Spectrum_neutron 3.92E+01 mBq/kg /cvmfs/lz.opensciencegrid.org/BACCARAT/SpectrumGenerator/Neutrons-18Jun19/PVDF_U_an_late_neutron.dat #~616 neutrons in 6 months
/Bacc/source/set Water_PMT_Can Spectrum_neutron 7.29E+01 mBq/kg /cvmfs/lz.opensciencegrid.org/BACCARAT/SpectrumGenerator/Neutrons-18Jun19/PVDF_Th_an_neutron.dat #~1165 neutrons in 6 months
/Bacc/source/set Water_PMT_Can Spectrum_neutron 4.40E-00 mBq/kg /cvmfs/lz.opensciencegrid.org/BACCARAT/SpectrumGenerator/Neutrons-18Jun19/SS316l_U_an_late_neutron.dat #~57 neutrons in 6 months, in lieu of OD PMT supports
/Bacc/source/set Water_PMT_Can Spectrum_neutron 5.40E-00 mBq/kg /cvmfs/lz.opensciencegrid.org/BACCARAT/SpectrumGenerator/Neutrons-18Jun19/SS316l_Th_an_neutron.dat #~70 neutrons in 6 months


#########################################################
# Start simulation
#########################################################
/control/getEnv NBEAMON
/Bacc/beamOn {NBEAMON}
exit
