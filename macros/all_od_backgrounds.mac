#########################################################
# Title: gdls_neutrons.mac
# Author: Sam Eriksen
# BACCARAT: release 6.2.11
# Sources: Gd-152 decay
#########################################################
/run/verbose       0
/control/verbose   0
/tracking/verbose  0

#########################################################
# IO
#########################################################
/Bacc/io/enableRootOutput 0
/Bacc/io/enableBinaryOutput 1

/control/getEnv OUTPUT_DIR
/Bacc/io/outputDir {OUTPUT_DIR}

/control/getEnv OUTPUT_FILENAME
/Bacc/io/outputName {OUTPUT_FILENAME}

/Bacc/io/alwaysRecordPrimary true

/Bacc/io/updateFrequency  1

#########################################################
# Set Record Levels
#########################################################
/Bacc/detector/recordLevel LiquidXenonTarget 5
/Bacc/detector/recordLevel BottomGridHolder 5
/Bacc/detector/recordLevel ReverseFieldRegion 5
/Bacc/detector/recordLevel CathodeGridHolder 5
/Bacc/detector/recordLevel GateGridHolder 5
/Bacc/detector/recordLevel LiquidSkinXenon 5
/Bacc/detector/recordLevel LiquidSkinXenonBank 5
/Bacc/detector/recordLevel ScintillatorCenter 5
/Bacc/detector/recordLevel InnerGaseousXenon 5
/Bacc/detector/recordLevel AnodeGridHolder 5
/Bacc/detector/recordLevel TopGridHolder 5
/Bacc/detector/recordLevel GaseousSkinXenon 5
/Bacc/detector/recordLevel GaseousSkinXenonBank 5
/Bacc/detector/recordLevel WaterAndPMTs 5

#########################################################
# Physics list
#########################################################
/Bacc/physicsList/useOptics true
/Bacc/physicsList/maxCereEleSteps 1000

/Bacc/physicsList/genTimesToZero true

/Bacc/physicsList/usePhotonEvaporation true

/run/initialize

/grdm/verbose      0
#########################################################
# Set the seed
#########################################################
/control/getEnv SEED
/Bacc/randomSeed {SEED}

#########################################################
# Detector setup
#########################################################
/Bacc/detector/select LZDetector

/LZ/gridWires off

/Bacc/detector/update

/Bacc/detector/useMCTruthLevels 1
#########################################################
# Material commands
#########################################################
/Bacc/materials/LXeTeflonRefl          0.973
/Bacc/materials/LXeAbsorption          100 m
/Bacc/materials/LXeSteelRefl           0.2
/Bacc/materials/GXeTeflonRefl          0.85
/Bacc/materials/GXeBlackWallRefl       0.2
/Bacc/materials/GXeSteelRefl           0.2
/Bacc/materials/GXeAbsorption          500 m
/Bacc/materials/AlUnoxidizedQuartzRefl 0.9
/Bacc/materials/GXeRayleigh            500 m
/Bacc/materials/LXeRayleigh            0.3 m
/Bacc/materials/QuartzAbsorption       1000 km
/Bacc/materials/LXeTeflonReflLiner    0.98
/Bacc/materials/LXeTeflonReflPMT      0.95
/Bacc/materials/LXeTeflonReflPlate    0.9
/Bacc/materials/LXeTeflonReflCables   0.1
/Bacc/materials/LXeTeflonReflTruss    0.8
/Bacc/materials/LXeTeflonReflBskinPMT 0.8
/Bacc/materials/LXeTeflonReflBplate   0.9
/Bacc/materials/LXeTeflonReflTskinPMT 0.8
/Bacc/materials/LXeTeflonReflWeir     0.1
/Bacc/materials/LXeTeflonReflDomePMT  0.8

#########################################################
# Set the Record Levels: Old BIN Format
#########################################################
/Bacc/detector/useMapOptical true

#########################################################
# Masses
#########################################################
/Bacc/detector/setComponentMass LiquidXenonTarget 7000 kg
# Xe Masses
/Bacc/detector/setComponentMass LiquidSkinXenon 1951.333 kg
/Bacc/detector/setComponentMass GaseousSkinXenon 1.95 kg
/Bacc/detector/setComponentMass GaseousSkinXenonBank 0.65 kg
/Bacc/detector/setComponentMass LiquidSkinXenonBank 650.419 kg
/Bacc/detector/setComponentMass LiquidXenonTarget 7000.410 kg
/Bacc/detector/setComponentMass InnerGaseousXenon 2.80 kg
/Bacc/detector/setComponentMass ReverseFieldRegion 659.457 kg
/Bacc/detector/setComponentMass GateGridHolder 0.736 kg
/Bacc/detector/setComponentMass CathodeGridHolder 0.976 kg
/Bacc/detector/setComponentMass BottomGridHolder 0.736 kg
/Bacc/detector/setComponentMass AnodeGridHolder 5.07E-03 kg
/Bacc/detector/setComponentMass ScintillatorCenter 17300 kg
# PMT Masses
/Bacc/detector/setComponentMass Top_PMT_Ceramic 1.570E-02 kg
/Bacc/detector/setComponentMass Bottom_PMT_Ceramic 1.570E-02 kg
/Bacc/detector/setComponentMass Top_PMT_Body 1.317E-01 kg
/Bacc/detector/setComponentMass Bottom_PMT_Body 1.317E-01 kg
/Bacc/detector/setComponentMass Top_PMT_Base 5.661E-03 kg
/Bacc/detector/setComponentMass Bottom_PMT_Base 5.661E-03 kg
/Bacc/detector/setComponentMass Dome_Skin_PMT_Body 1.267E-01 kg
/Bacc/detector/setComponentMass Bottom_Skin_PMT_Body 1.267E-01 kg
/Bacc/detector/setComponentMass Top_Skin_PMT_Body 2.397E-02 kg
/Bacc/detector/setComponentMass Water_PMT_Window 1.02 kg
/Bacc/detector/setComponentMass Water_PMT_Can 0.686 kg
# PTFE Masses
/Bacc/detector/setComponentMass PTFEWallsInLiquid 184.0 kg
/Bacc/detector/setComponentMass PTFELinerLiquid 26.224 kg #Skin teflon outer liner
/Bacc/detector/setComponentMass TopPTFELiner 5.0278 kg #Upper PMT Structure -> Trifoils, PMT sleeves etc. for top array
/Bacc/detector/setComponentMass BottomPTFELiner 1.3527 kg #Lower PMT Structure -> Trifoils  for bottom array
/Bacc/detector/setComponentMass BottomXeSkinPTFELiner 66.7273 kg #Lower PMT Structure -> Dome skin PODs etc. pointing into skin dome
/Bacc/detector/setComponentMass TopSkinIceCubeTray 10.565 kg #This is *not* in the backgrounds table. Going to assume the same specs as TopPTFELiner
# Cryostat Masses
/Bacc/detector/setComponentMass InnerTitaniumVessel 800 kg
/Bacc/detector/setComponentMass OuterTitaniumVessel 1022 kg
/Bacc/detector/setComponentMass TitaniumLegs 454 kg
/Bacc/detector/setComponentMass FoamInsulation 13.781 kg
/Bacc/detector/setComponentMass AluminumSealantInInnerVesselFlange 7.37E-01 kg #(Al+Iconel+Niconic) The following are guesses,
/Bacc/detector/setComponentMass ElastomerSealantInInnerVesselFlange 2.29E-01 kg  #using the previous ratio of masses to divide
/Bacc/detector/setComponentMass ElastomerSealantInOuterVesselUpperFlange 5.74E-01 kg #up mass from backgrounds table
/Bacc/detector/setComponentMass ElastomerSealantInOuterVesselBottomFlange 5.74E-01 kg
/Bacc/detector/setComponentMass BoltsInInnerVesselFlange 23 kg #Bolts take on all 53.44 kg of Stainless Steel...
/Bacc/detector/setComponentMass BoltsInOuterVesselUpperFlange 15.22 kg
/Bacc/detector/setComponentMass BoltsInOuterVesselBottomFlange 15.22 kg
# Truss Masses - these absorb everything for lower/upper PMT structure, including PTFE mass that is separately used for PTFEAlphaN
/Bacc/detector/setComponentMass BottomTruss 53.625 kg
/Bacc/detector/setComponentMass TopTruss 115.143 kg
# Conduit Mass - using this to represent the entire Xe recirculation tubing
/Bacc/detector/setComponentMass BottomConduit 14.09 kg
# Outer Detector Masses
/Bacc/detector/setComponentMass ScintillatorTank 3164 kg #Acrylic + Displacement Foam
/Bacc/detector/setComponentMass SteelLegs 1100 kg
# Field Ring Masses
/Bacc/detector/setComponentMass ForwardFieldRing 1.3464 kg
/Bacc/detector/setComponentMass ReverseFieldRing 1.35 kg
# Grid Holder Masses
/Bacc/detector/setComponentMass AnodePhysGrid 19.94 kg
/Bacc/detector/setComponentMass BottomPhysGrid 11.5 kg
/Bacc/detector/setComponentMass CathodePhysGrid 17.9 kg
/Bacc/detector/setComponentMass GatePhysGrid 33.35 kg
# HV Conduit Masses
/Bacc/detector/setComponentMass HVConduitVacuumInnerSteelCone 7.70 kg  #Going again by the ratio of inner to outer cones in the TDR macro
/Bacc/detector/setComponentMass HVConduitInnerOuterSteelCone 121.30 kg  #Going again by the ratio of inner to outer cones in the TDR macro
/Bacc/detector/setComponentMass HVConduitInnerConeInnerStructure 14.87 kg
/Bacc/detector/setComponentMass HVHolePTFELinerLiquid 0.905 kg

#########################################################
# Sources
#########################################################
/Bacc/source/set Top_PMT_Body DecayChain_238_92_stop_226_88 118.42 mBq/kg EQUILIBRIUM
/Bacc/source/set Top_PMT_Body DecayChain_226_88 9.69 mBq/kg EQUILIBRIUM
/Bacc/source/set Top_PMT_Body DecayChain_232_90 10.26 mBq/kg EQUILIBRIUM
/Bacc/source/set Top_PMT_Body G4Decay_60_27 2.99 mBq/kg
/Bacc/source/set Top_PMT_Body G4Decay_40_19 1.621 mBq/kg
/Bacc/source/set Bottom_PMT_Body DecayChain_238_92_stop_226_88 118.42 mBq/kg EQUILIBRIUM
/Bacc/source/set Bottom_PMT_Body DecayChain_226_88 9.69 mBq/kg EQUILIBRIUM
/Bacc/source/set Bottom_PMT_Body DecayChain_232_90 10.26 mBq/kg EQUILIBRIUM
/Bacc/source/set Bottom_PMT_Body G4Decay_60_27 2.99 mBq/kg
/Bacc/source/set Bottom_PMT_Body G4Decay_40_19 1.621 mBq/kg
/Bacc/source/set Top_Skin_PMT_Body DecayChain_238_92_stop_226_88 1236.56 mBq/kg EQUILIBRIUM
/Bacc/source/set Top_Skin_PMT_Body DecayChain_226_88 412.18 mBq/kg EQUILIBRIUM
/Bacc/source/set Top_Skin_PMT_Body DecayChain_232_90 305.73 mBq/kg EQUILIBRIUM
/Bacc/source/set Top_Skin_PMT_Body G4Decay_60_27 23.80 mBq/kg
/Bacc/source/set Top_Skin_PMT_Body G4Decay_40_19 186.32 mBq/kg
/Bacc/source/set Bottom_Skin_PMT_Body DecayChain_238_92_stop_226_88 6.76 mBq/kg EQUILIBRIUM
/Bacc/source/set Bottom_Skin_PMT_Body DecayChain_226_88 5.26 mBq/kg EQUILIBRIUM
/Bacc/source/set Bottom_Skin_PMT_Body DecayChain_232_90 1.67 mBq/kg EQUILIBRIUM
/Bacc/source/set Bottom_Skin_PMT_Body G4Decay_60_27 0.16 mBq/kg
/Bacc/source/set Bottom_Skin_PMT_Body G4Decay_40_19 4.10 mBq/kg
/Bacc/source/set Dome_Skin_PMT_Body DecayChain_238_92_stop_226_88 6.76 mBq/kg EQUILIBRIUM
/Bacc/source/set Dome_Skin_PMT_Body DecayChain_226_88 5.26 mBq/kg EQUILIBRIUM
/Bacc/source/set Dome_Skin_PMT_Body DecayChain_232_90 1.67 mBq/kg EQUILIBRIUM
/Bacc/source/set Dome_Skin_PMT_Body G4Decay_60_27 0.16 mBq/kg
/Bacc/source/set Dome_Skin_PMT_Body G4Decay_40_19 4.10 mBq/kg
/Bacc/source/set Top_PMT_Base DecayChain_238_92_stop_226_88 1293.67 mBq/kg EQUILIBRIUM
/Bacc/source/set Top_PMT_Base DecayChain_226_88 477.98 mBq/kg EQUILIBRIUM
/Bacc/source/set Top_PMT_Base DecayChain_232_90 35.99 mBq/kg EQUILIBRIUM
/Bacc/source/set Top_PMT_Base G4Decay_60_27 0.97 mBq/kg
/Bacc/source/set Top_PMT_Base G4Decay_40_19 5.06 mBq/kg
/Bacc/source/set Bottom_PMT_Base DecayChain_238_92_stop_226_88 1293.67 mBq/kg EQUILIBRIUM
/Bacc/source/set Bottom_PMT_Base DecayChain_226_88 477.98 mBq/kg EQUILIBRIUM
/Bacc/source/set Bottom_PMT_Base DecayChain_232_90 35.99 mBq/kg EQUILIBRIUM
/Bacc/source/set Bottom_PMT_Base G4Decay_60_27 0.97 mBq/kg
/Bacc/source/set Bottom_PMT_Base G4Decay_40_19 5.06 mBq/kg
/Bacc/source/set TopTruss DecayChain_238_92_stop_226_88 41.09 mBq/kg EQUILIBRIUM
/Bacc/source/set TopTruss DecayChain_226_88 44.43 mBq/kg EQUILIBRIUM
/Bacc/source/set TopTruss DecayChain_232_90 24.00 mBq/kg EQUILIBRIUM
/Bacc/source/set TopTruss G4Decay_60_27 0.31 mBq/kg
/Bacc/source/set TopTruss G4Decay_40_19 2.11 mBq/kg
/Bacc/source/set BottomTruss DecayChain_238_92_stop_226_88 15.46 mBq/kg EQUILIBRIUM
/Bacc/source/set BottomTruss DecayChain_226_88 18.94 mBq/kg EQUILIBRIUM
/Bacc/source/set BottomTruss DecayChain_232_90 9.73 mBq/kg EQUILIBRIUM
/Bacc/source/set BottomTruss G4Decay_60_27 0.15 mBq/kg
/Bacc/source/set BottomTruss G4Decay_40_19 1.51 mBq/kg
/Bacc/source/set PTFEWallsInLiquid DecayChain_238_92 0.53 mBq/kg EQUILIBRIUM
/Bacc/source/set PTFEWallsInLiquid DecayChain_226_88 0.95 mBq/kg EQUILIBRIUM
/Bacc/source/set PTFEWallsInLiquid DecayChain_232_90 0.33 mBq/kg EQUILIBRIUM
/Bacc/source/set PTFEWallsInLiquid G4Decay_40_19 0.066 mBq/kg
/Bacc/source/set AnodePhysGrid DecayChain_238_92_stop_226_88 43.82 mBq/kg EQUILIBRIUM
/Bacc/source/set AnodePhysGrid DecayChain_226_88 29.91 mBq/kg EQUILIBRIUM
/Bacc/source/set AnodePhysGrid DecayChain_232_90 16.12 mBq/kg EQUILIBRIUM
/Bacc/source/set AnodePhysGrid G4Decay_60_27 5.37 mBq/kg
/Bacc/source/set AnodePhysGrid G4Decay_40_19 2.05 mBq/kg
/Bacc/source/set GatePhysGrid DecayChain_238_92_stop_226_88 43.82 mBq/kg EQUILIBRIUM
/Bacc/source/set GatePhysGrid DecayChain_226_88 29.91 mBq/kg EQUILIBRIUM
/Bacc/source/set GatePhysGrid DecayChain_232_90 16.12 mBq/kg EQUILIBRIUM
/Bacc/source/set GatePhysGrid G4Decay_60_27 5.37 mBq/kg
/Bacc/source/set GatePhysGrid G4Decay_40_19 2.05 mBq/kg
/Bacc/source/set CathodePhysGrid DecayChain_238_92_stop_226_88 43.82 mBq/kg EQUILIBRIUM
/Bacc/source/set CathodePhysGrid DecayChain_226_88 29.91 mBq/kg EQUILIBRIUM
/Bacc/source/set CathodePhysGrid DecayChain_232_90 16.12 mBq/kg EQUILIBRIUM
/Bacc/source/set CathodePhysGrid G4Decay_60_27 5.37 mBq/kg
/Bacc/source/set CathodePhysGrid G4Decay_40_19 2.05 mBq/kg
/Bacc/source/set BottomPhysGrid DecayChain_238_92_stop_226_88 43.82 mBq/kg EQUILIBRIUM
/Bacc/source/set BottomPhysGrid DecayChain_226_88 29.91 mBq/kg EQUILIBRIUM
/Bacc/source/set BottomPhysGrid DecayChain_232_90 16.12 mBq/kg EQUILIBRIUM
/Bacc/source/set BottomPhysGrid G4Decay_60_27 5.37 mBq/kg
/Bacc/source/set BottomPhysGrid G4Decay_40_19 2.05 mBq/kg
/Bacc/source/set ForwardFieldRing DecayChain_238_92_stop_226_88 22.14 mBq/kg EQUILIBRIUM
/Bacc/source/set ForwardFieldRing DecayChain_226_88 19.77 mBq/kg EQUILIBRIUM
/Bacc/source/set ForwardFieldRing DecayChain_232_90 23.94 mBq/kg EQUILIBRIUM
/Bacc/source/set ForwardFieldRing G4Decay_40_19 1.94 mBq/kg
/Bacc/source/set ReverseFieldRing DecayChain_238_92_stop_226_88 22.14 mBq/kg EQUILIBRIUM
/Bacc/source/set ReverseFieldRing DecayChain_226_88 19.77 mBq/kg EQUILIBRIUM
/Bacc/source/set ReverseFieldRing DecayChain_232_90 23.94 mBq/kg EQUILIBRIUM
/Bacc/source/set ReverseFieldRing G4Decay_40_19 1.94 mBq/kg
/Bacc/source/set BottomConduit DecayChain_238_92_stop_226_88 3.97 mBq/kg EQUILIBRIUM
/Bacc/source/set BottomConduit DecayChain_226_88 3.00 mBq/kg EQUILIBRIUM
/Bacc/source/set BottomConduit DecayChain_232_90 1.81 mBq/kg EQUILIBRIUM
/Bacc/source/set BottomConduit G4Decay_60_27 1.522 mBq/kg
/Bacc/source/set BottomConduit G4Decay_40_19 1.522 mBq/kg
/Bacc/source/set InnerTitaniumVessel DecayChain_238_92_stop_226_88 8.89E-02 mBq/kg EQUILIBRIUM
/Bacc/source/set InnerTitaniumVessel DecayChain_226_88 3.82E-02 mBq/kg EQUILIBRIUM
/Bacc/source/set InnerTitaniumVessel DecayChain_232_90 1.03E-02 mBq/kg EQUILIBRIUM
/Bacc/source/set InnerTitaniumVessel G4Decay_60_27 2.76E-02 mBq/kg
/Bacc/source/set InnerTitaniumVessel G4Decay_40_19 2.25E-02 mBq/kg
/Bacc/source/set OuterTitaniumVessel DecayChain_238_92_stop_226_88 8.89E-02 mBq/kg EQUILIBRIUM
/Bacc/source/set OuterTitaniumVessel DecayChain_226_88 3.82E-02 mBq/kg EQUILIBRIUM
/Bacc/source/set OuterTitaniumVessel DecayChain_232_90 1.03E-02 mBq/kg EQUILIBRIUM
/Bacc/source/set OuterTitaniumVessel G4Decay_60_27 2.76E-02 mBq/kg
/Bacc/source/set OuterTitaniumVessel G4Decay_40_19 2.25E-02 mBq/kg
/Bacc/source/set ElastomerSealantInInnerVesselFlange DecayChain_238_92_stop_226_88 3.07 mBq/kg EQUILIBRIUM
/Bacc/source/set ElastomerSealantInInnerVesselFlange DecayChain_226_88 0.34 mBq/kg EQUILIBRIUM
/Bacc/source/set ElastomerSealantInInnerVesselFlange DecayChain_232_90 4.58 mBq/kg EQUILIBRIUM
/Bacc/source/set ElastomerSealantInInnerVesselFlange G4Decay_60_27 2.76 mBq/kg
/Bacc/source/set ElastomerSealantInInnerVesselFlange G4Decay_40_19 1.42 mBq/kg
/Bacc/source/set ElastomerSealantInOuterVesselUpperFlange DecayChain_238_92_stop_226_88 3.07 mBq/kg EQUILIBRIUM
/Bacc/source/set ElastomerSealantInOuterVesselUpperFlange DecayChain_226_88 0.34 mBq/kg EQUILIBRIUM
/Bacc/source/set ElastomerSealantInOuterVesselUpperFlange DecayChain_232_90 4.58 mBq/kg EQUILIBRIUM
/Bacc/source/set ElastomerSealantInOuterVesselUpperFlange G4Decay_60_27 2.76 mBq/kg
/Bacc/source/set ElastomerSealantInOuterVesselUpperFlange G4Decay_40_19 1.42 mBq/kg
/Bacc/source/set ElastomerSealantInOuterVesselBottomFlange DecayChain_238_92_stop_226_88 3.07 mBq/kg EQUILIBRIUM
/Bacc/source/set ElastomerSealantInOuterVesselBottomFlange DecayChain_226_88 0.34 mBq/kg EQUILIBRIUM
/Bacc/source/set ElastomerSealantInOuterVesselBottomFlange DecayChain_232_90 4.58 mBq/kg EQUILIBRIUM
/Bacc/source/set ElastomerSealantInOuterVesselBottomFlange G4Decay_60_27 2.76 mBq/kg
/Bacc/source/set ElastomerSealantInOuterVesselBottomFlange G4Decay_40_19 1.42 mBq/kg
/Bacc/source/set FoamInsulation DecayChain_238_92_stop_226_88 18.93 mBq/kg EQUILIBRIUM
/Bacc/source/set FoamInsulation DecayChain_226_88 5.01 mBq/kg EQUILIBRIUM
/Bacc/source/set FoamInsulation DecayChain_232_90 5.39 mBq/kg EQUILIBRIUM
/Bacc/source/set FoamInsulation G4Decay_60_27 8.68E-02 mBq/kg
/Bacc/source/set FoamInsulation G4Decay_40_19 4.06 mBq/kg
/Bacc/source/set PTFELinerLiquid DecayChain_238_92_stop_226_88 16.64 mBq/kg EQUILIBRIUM
/Bacc/source/set PTFELinerLiquid DecayChain_226_88 11.16 mBq/kg EQUILIBRIUM
/Bacc/source/set PTFELinerLiquid DecayChain_232_90 16.71 mBq/kg EQUILIBRIUM
/Bacc/source/set PTFELinerLiquid G4Decay_60_27 1.11 mBq/kg
/Bacc/source/set PTFELinerLiquid G4Decay_40_19 1.51 mBq/kg
/Bacc/source/set ScintillatorTank DecayChain_238_92_stop_226_88 1.17E-02 mBq/kg EQUILIBRIUM
/Bacc/source/set ScintillatorTank DecayChain_226_88 2.42E-03 mBq/kg EQUILIBRIUM
/Bacc/source/set ScintillatorTank DecayChain_232_90 4.00E-03 mBq/kg EQUILIBRIUM
/Bacc/source/set ScintillatorTank G4Decay_40_19 4.06E-03 mBq/kg
/Bacc/source/set Water_PMT_Can DecayChain_238_92_stop_226_88 1.92E-02 mBq/kg EQUILIBRIUM
/Bacc/source/set Water_PMT_Can DecayChain_226_88 2.85E-02 mBq/kg EQUILIBRIUM
/Bacc/source/set Water_PMT_Can DecayChain_232_90 2.64E-02 mBq/kg EQUILIBRIUM
/Bacc/source/set Water_PMT_Can G4Decay_40_19 3.59E-03 mBq/kg
/Bacc/source/set PTFEWallsInLiquid PTFEAlphaN 4.58E-07 mBq/kg Uearly
/Bacc/source/set PTFEWallsInLiquid PTFEAlphaN 2.48E-06 mBq/kg Ulate
/Bacc/source/set PTFEWallsInLiquid PTFEAlphaN 1.12E-06 mBq/kg Th
/Bacc/source/set PTFEWallsInLiquid PTFEAlphaN 8.88E-06 mBq/kg Po210
/Bacc/source/set PTFELinerLiquid PTFEAlphaN 1.00E-05 mBq/kg Uearly
/Bacc/source/set PTFELinerLiquid PTFEAlphaN 1.59E-05 mBq/kg Ulate
/Bacc/source/set PTFELinerLiquid PTFEAlphaN 3.65E-05 mBq/kg Th
/Bacc/source/set PTFELinerLiquid PTFEAlphaN 8.88E-06 mBq/kg Po210
/Bacc/source/set TopPTFELiner PTFEAlphaN 3.13E-05 mBq/kg Uearly
/Bacc/source/set TopPTFELiner PTFEAlphaN 6.03E-05 mBq/kg Ulate
/Bacc/source/set TopPTFELiner PTFEAlphaN 5.94E-05 mBq/kg Th
/Bacc/source/set TopPTFELiner PTFEAlphaN 8.88E-06 mBq/kg Po210
/Bacc/source/set BottomXeSkinPTFELiner PTFEAlphaN 8.89E-06 mBq/kg Uearly
/Bacc/source/set BottomXeSkinPTFELiner PTFEAlphaN 2.75E-05 mBq/kg Ulate
/Bacc/source/set BottomXeSkinPTFELiner PTFEAlphaN 3.55E-05 mBq/kg Th
/Bacc/source/set BottomXeSkinPTFELiner PTFEAlphaN 1.27E-05 mBq/kg Po210
/Bacc/source/set TopSkinIceCubeTray PTFEAlphaN 3.13E-05 mBq/kg Uearly
/Bacc/source/set TopSkinIceCubeTray PTFEAlphaN 6.03E-05 mBq/kg Ulate
/Bacc/source/set TopSkinIceCubeTray PTFEAlphaN 5.94E-05 mBq/kg Th
/Bacc/source/set TopSkinIceCubeTray PTFEAlphaN 8.88E-06 mBq/kg Po210
/Bacc/source/set ScintillatorTank PTFEAlphaN 1.77E-07 mBq/kg Uearly
/Bacc/source/set ScintillatorTank PTFEAlphaN 3.40E-07 mBq/kg Ulate
/Bacc/source/set ScintillatorTank PTFEAlphaN 4.27E-07 mBq/kg Th
/Bacc/source/set ScintillatorTank PTFEAlphaN 7.30E-09 mBq/kg Po210
/Bacc/source/set BottomConduit PTFEAlphaN 7.05E-06 mBq/kg Uearly
/Bacc/source/set BottomConduit PTFEAlphaN 1.40E-05 mBq/kg Ulate
/Bacc/source/set BottomConduit PTFEAlphaN 3.18E-05 mBq/kg Th
/Bacc/source/set BottomConduit PTFEAlphaN 3.33E-06 mBq/kg Po210
/Bacc/source/set HVHolePTFELinerLiquid PTFEAlphaN 3.37E-04 mBq/kg Uearly
/Bacc/source/set HVHolePTFELinerLiquid PTFEAlphaN 5.32E-04 mBq/kg Ulate
/Bacc/source/set HVHolePTFELinerLiquid PTFEAlphaN 8.62E-04 mBq/kg Th
/Bacc/source/set HVHolePTFELinerLiquid PTFEAlphaN 8.88E-06 mBq/kg Po210
/Bacc/source/set ScintillatorCenter DecayChain_238_92_stop_226_88 1.35 mBq/kg EQUILIBRIUM
/Bacc/source/set ScintillatorCenter DecayChain_226_88 2.43 mBq/kg EQUILIBRIUM
/Bacc/source/set ScintillatorCenter DecayChain_232_90 3.29 mBq/kg EQUILIBRIUM
/Bacc/source/set ScintillatorCenter SingleDecay_152_64 1.6 mBq/kg
/Bacc/source/set ScintillatorCenter G4Decay_14_6 4.6 mBq/kg
/Bacc/source/set ScintillatorCenter G4Decay_227_89 2E-02 mBq/kg
/Bacc/source/set ScintillatorCenter SingleDecay_176_71 7E-03 mBq/kg
/Bacc/source/set Top_PMT_Ceramic USF 5.65E-05 mBq/kg
/Bacc/source/set Bottom_PMT_Ceramic USF 5.65E-05 mBq/kg
/Bacc/source/set Top_PMT_Body USF 4.50E-05 mBq/kg
/Bacc/source/set Bottom_PMT_Body USF 4.50E-05 mBq/kg
/Bacc/source/set Bottom_Skin_PMT_Body USF 9.00E-05 mBq/kg
/Bacc/source/set Dome_Skin_PMT_Body USF 9.00E-05 mBq/kg
/Bacc/source/set Top_PMT_Base USF 2.63E-04 mBq/kg
/Bacc/source/set Bottom_PMT_Base USF 2.63E-04 mBq/kg
/Bacc/source/set ScintillatorTank USF 4.44E-07 mBq/kg
/Bacc/source/set SteelLegs USF 6.71E-06 mBq/kg
/Bacc/source/set Water_PMT_Window USF 2.17E-04 mBq/kg
/Bacc/source/set Water_PMT_Can USF 1.82E-04 mBq/kg
/Bacc/source/set InnerTitaniumVessel USF 5.66E-05 mBq/kg
/Bacc/source/set OuterTitaniumVessel USF 5.66E-05 mBq/kg
/Bacc/source/set TitaniumLegs USF 5.66E-05 mBq/kg
/Bacc/source/set FoamInsulation USF 1.65E-04 mBq/kg
/Bacc/source/set BoltsInInnerVesselFlange USF 2.69E-05 mBq/kg
/Bacc/source/set BoltsInOuterVesselUpperFlange USF 2.69E-05 mBq/kg
/Bacc/source/set BoltsInOuterVesselBottomFlange USF 2.69E-05 mBq/kg
/Bacc/source/set AluminumSealantInInnerVesselFlange USF 3.33E-04 mBq/kg
/Bacc/source/set ElastomerSealantInInnerVesselFlange USF 1.2E-04 mBq/kg
/Bacc/source/set ElastomerSealantInOuterVesselUpperFlange USF 1.22E-04 mBq/kg
/Bacc/source/set ElastomerSealantInOuterVesselUpperFlange USF 1.22E-04 mBq/kg
/Bacc/source/set AnodePhysGrid USF 3.66E-06 mBq/kg
/Bacc/source/set BottomPhysGrid USF 3.66E-06 mBq/kg
/Bacc/source/set CathodePhysGrid USF 3.66E-06 mBq/kg
/Bacc/source/set GatePhysGrid USF 3.66E-06 mBq/kg
/Bacc/source/set HVConduitVacuumInnerSteelCone USF 2.23E-06 mBq/kg
/Bacc/source/set HVConduitInnerOuterSteelCone USF 1.41E-05 mBq/kg
/Bacc/source/set HVConduitInnerConeInnerStructure USF 1.41E-05 mBq/kg
/Bacc/source/set Top_PMT_Ceramic Spectrum_neutron 5.99E-05 mBq/kg /cvmfs/lz.opensciencegrid.org/BACCARAT/SpectrumGenerator/Neutrons-18Jun19/Al2O3_U_an_early_neutron.dat
/Bacc/source/set Top_PMT_Ceramic Spectrum_neutron 1.20E-04 mBq/kg /cvmfs/lz.opensciencegrid.org/BACCARAT/SpectrumGenerator/Neutrons-18Jun19/Al2O3_U_an_late_neutron.dat
/Bacc/source/set Top_PMT_Ceramic Spectrum_neutron 9.72E-05 mBq/kg /cvmfs/lz.opensciencegrid.org/BACCARAT/SpectrumGenerator/Neutrons-18Jun19/Al2O3_Th_an_neutron.dat
/Bacc/source/set Bottom_PMT_Ceramic Spectrum_neutron 5.99E-05 mBq/kg /cvmfs/lz.opensciencegrid.org/BACCARAT/SpectrumGenerator/Neutrons-18Jun19/Al2O3_U_an_early_neutron.dat
/Bacc/source/set Bottom_PMT_Ceramic Spectrum_neutron 1.20E-04 mBq/kg /cvmfs/lz.opensciencegrid.org/BACCARAT/SpectrumGenerator/Neutrons-18Jun19/Al2O3_U_an_late_neutron.dat
/Bacc/source/set Bottom_PMT_Ceramic Spectrum_neutron 9.72E-05 mBq/kg /cvmfs/lz.opensciencegrid.org/BACCARAT/SpectrumGenerator/Neutrons-18Jun19/Al2O3_Th_an_neutron.dat
/Bacc/source/set Top_Skin_PMT_Body Spectrum_neutron 1.15E-04 mBq/kg /cvmfs/lz.opensciencegrid.org/BACCARAT/SpectrumGenerator/Neutrons-18Jun19/SiO2_U_an_early_neutron.dat
/Bacc/source/set Top_Skin_PMT_Body Spectrum_neutron 3.93E-04 mBq/kg /cvmfs/lz.opensciencegrid.org/BACCARAT/SpectrumGenerator/Neutrons-18Jun19/SiO2_U_an_late_neutron.dat
/Bacc/source/set Top_Skin_PMT_Body Spectrum_neutron 1.88E-04 mBq/kg /cvmfs/lz.opensciencegrid.org/BACCARAT/SpectrumGenerator/Neutrons-18Jun19/SiO2_Th_an_neutron.dat
/Bacc/source/set Bottom_Skin_PMT_Body Spectrum_neutron 1.84E-04 mBq/kg /cvmfs/lz.opensciencegrid.org/BACCARAT/SpectrumGenerator/Neutrons-18Jun19/Al2O3_U_an_late_neutron.dat
/Bacc/source/set Bottom_Skin_PMT_Body Spectrum_neutron 4.64E-05 mBq/kg /cvmfs/lz.opensciencegrid.org/BACCARAT/SpectrumGenerator/Neutrons-18Jun19/Al2O3_Th_an_neutron.dat
/Bacc/source/set Top_PMT_Base Spectrum_neutron 1.41E-04 mBq/kg /cvmfs/lz.opensciencegrid.org/BACCARAT/SpectrumGenerator/Neutrons-18Jun19/Al2O3_U_an_late_neutron.dat
/Bacc/source/set Bottom_PMT_Base Spectrum_neutron 1.41E-04 mBq/kg /cvmfs/lz.opensciencegrid.org/BACCARAT/SpectrumGenerator/Neutrons-18Jun19/Al2O3_U_an_late_neutron.dat
/Bacc/source/set PTFEWallsInLiquid Spectrum_neutron 3.60E-06 mBq/kg /cvmfs/lz.opensciencegrid.org/BACCARAT/SpectrumGenerator/Spectra-31May19/NeutronSpectrum_PTFE.dat
/Bacc/source/set ForwardFieldRing Spectrum_neutron 5.08E-06 mBq/kg /cvmfs/lz.opensciencegrid.org/BACCARAT/SpectrumGenerator/Neutrons-18Jun19/Al2O3_U_an_late_neutron.dat
/Bacc/source/set ForwardFieldRing Spectrum_neutron 1.29E-05 mBq/kg /cvmfs/lz.opensciencegrid.org/BACCARAT/SpectrumGenerator/Neutrons-18Jun19/Al2O3_Th_an_neutron.dat
/Bacc/source/set ReverseFieldRing Spectrum_neutron 5.08E-06 mBq/kg /cvmfs/lz.opensciencegrid.org/BACCARAT/SpectrumGenerator/Neutrons-18Jun19/Al2O3_U_an_late_neutron.dat
/Bacc/source/set ReverseFieldRing Spectrum_neutron 1.29E-05 mBq/kg /cvmfs/lz.opensciencegrid.org/BACCARAT/SpectrumGenerator/Neutrons-18Jun19/Al2O3_Th_an_neutron.dat
/Bacc/source/set InnerTitaniumVessel Spectrum_neutron 7.98E-08 mBq/kg /cvmfs/lz.opensciencegrid.org/BACCARAT/SpectrumGenerator/Neutrons-18Jun19/Titanium_U_an_late_neutron.dat
/Bacc/source/set InnerTitaniumVessel Spectrum_neutron 3.45E-07 mBq/kg /cvmfs/lz.opensciencegrid.org/BACCARAT/SpectrumGenerator/Neutrons-18Jun19/Titanium_Th_an_neutron.dat
/Bacc/source/set OuterTitaniumVessel Spectrum_neutron 7.98E-08 mBq/kg /cvmfs/lz.opensciencegrid.org/BACCARAT/SpectrumGenerator/Neutrons-18Jun19/Titanium_U_an_late_neutron.dat
/Bacc/source/set OuterTitaniumVessel Spectrum_neutron 3.45E-07 mBq/kg /cvmfs/lz.opensciencegrid.org/BACCARAT/SpectrumGenerator/Neutrons-18Jun19/Titanium_Th_an_neutron.dat
/Bacc/source/set TitaniumLegs Spectrum_neutron 7.98E-08 mBq/kg /cvmfs/lz.opensciencegrid.org/BACCARAT/SpectrumGenerator/Neutrons-18Jun19/Titanium_U_an_late_neutron.dat
/Bacc/source/set TitaniumLegs Spectrum_neutron 3.45E-07 mBq/kg /cvmfs/lz.opensciencegrid.org/BACCARAT/SpectrumGenerator/Neutrons-18Jun19/Titanium_Th_an_neutron.dat
/Bacc/source/set BoltsInInnerVesselFlange Spectrum_neutron 5.73E-06 mBq/kg /cvmfs/lz.opensciencegrid.org/BACCARAT/SpectrumGenerator/Neutrons-18Jun19/SS316l_Th_an_neutron.dat
/Bacc/source/set BoltsInOuterVesselUpperFlange Spectrum_neutron 5.73E-06 mBq/kg /cvmfs/lz.opensciencegrid.org/BACCARAT/SpectrumGenerator/Neutrons-18Jun19/SS316l_Th_an_neutron.dat
/Bacc/source/set BoltsInOuterVesselBottomFlange Spectrum_neutron 5.73E-06 mBq/kg /cvmfs/lz.opensciencegrid.org/BACCARAT/SpectrumGenerator/Neutrons-18Jun19/SS316l_Th_an_neutron.dat
/Bacc/source/set ScintillatorTank Spectrum_neutron 1.62E-07 mBq/kg /cvmfs/lz.opensciencegrid.org/BACCARAT/SpectrumGenerator/Neutrons-18Jun19/polyethylene_U_an_early_neutron.dat
/Bacc/source/set ScintillatorTank Spectrum_neutron 8.08E-08 mBq/kg /cvmfs/lz.opensciencegrid.org/BACCARAT/SpectrumGenerator/Neutrons-18Jun19/polyethylene_U_an_late_neutron.dat
/Bacc/source/set ScintillatorTank Spectrum_neutron 1.23E-07 mBq/kg /cvmfs/lz.opensciencegrid.org/BACCARAT/SpectrumGenerator/Neutrons-18Jun19/polyethylene_Th_an_neutron.dat
/Bacc/source/set SteelLegs Spectrum_neutron 1.79E-07 mBq/kg /cvmfs/lz.opensciencegrid.org/BACCARAT/SpectrumGenerator/Neutrons-18Jun19/SS316l_U_an_early_neutron.dat
/Bacc/source/set SteelLegs Spectrum_neutron 4.70E-07 mBq/kg /cvmfs/lz.opensciencegrid.org/BACCARAT/SpectrumGenerator/Neutrons-18Jun19/SS316l_U_an_late_neutron.dat
/Bacc/source/set SteelLegs Spectrum_neutron 2.89E-06 mBq/kg /cvmfs/lz.opensciencegrid.org/BACCARAT/SpectrumGenerator/Neutrons-18Jun19/SS316l_Th_an_neutron.dat
/Bacc/source/set Water_PMT_Window Spectrum_neutron 6.45E-05 mBq/kg /cvmfs/lz.opensciencegrid.org/BACCARAT/SpectrumGenerator/Neutrons-18Jun19/SiO2_U_an_early_neutron.dat
/Bacc/source/set Water_PMT_Window Spectrum_neutron 4.46E-04 mBq/kg /cvmfs/lz.opensciencegrid.org/BACCARAT/SpectrumGenerator/Neutrons-18Jun19/SiO2_U_an_late_neutron.dat
/Bacc/source/set Water_PMT_Window Spectrum_neutron 3.46E-04 mBq/kg /cvmfs/lz.opensciencegrid.org/BACCARAT/SpectrumGenerator/Neutrons-18Jun19/SiO2_Th_an_neutron.dat
/Bacc/source/set Water_PMT_Can Spectrum_neutron 1.39E-05 mBq/kg /cvmfs/lz.opensciencegrid.org/BACCARAT/SpectrumGenerator/Neutrons-18Jun19/polyethylene_U_an_early_neutron.dat
/Bacc/source/set Water_PMT_Can Spectrum_neutron 7.27E-05 mBq/kg /cvmfs/lz.opensciencegrid.org/BACCARAT/SpectrumGenerator/Neutrons-18Jun19/polyethylene_U_an_late_neutron.dat
/Bacc/source/set Water_PMT_Can Spectrum_neutron 3.28E-05 mBq/kg /cvmfs/lz.opensciencegrid.org/BACCARAT/SpectrumGenerator/Neutrons-18Jun19/polyethylene_Th_an_neutron.dat
/Bacc/source/set Water_PMT_Can Spectrum_neutron 1.32E-04 mBq/kg /cvmfs/lz.opensciencegrid.org/BACCARAT/SpectrumGenerator/Neutrons-18Jun19/PVDF_U_an_early_neutron.dat
/Bacc/source/set Water_PMT_Can Spectrum_neutron 3.92E-04 mBq/kg /cvmfs/lz.opensciencegrid.org/BACCARAT/SpectrumGenerator/Neutrons-18Jun19/PVDF_U_an_late_neutron.dat
/Bacc/source/set Water_PMT_Can Spectrum_neutron 7.29E-04 mBq/kg /cvmfs/lz.opensciencegrid.org/BACCARAT/SpectrumGenerator/Neutrons-18Jun19/PVDF_Th_an_neutron.dat
/Bacc/source/set Water_PMT_Can Spectrum_neutron 4.40E-05 mBq/kg /cvmfs/lz.opensciencegrid.org/BACCARAT/SpectrumGenerator/Neutrons-18Jun19/SS316l_U_an_late_neutron.dat
/Bacc/source/set Water_PMT_Can Spectrum_neutron 5.40E-05 mBq/kg /cvmfs/lz.opensciencegrid.org/BACCARAT/SpectrumGenerator/Neutrons-18Jun19/SS316l_Th_an_neutron.dat
/Bacc/source/set RockGammas 2.91 Bq U238
/Bacc/source/set RockGammas 1.25 Bq Th232
/Bacc/source/set RockGammas 2.16 Bq K40
#########################################################
# Start simulation
#########################################################
/control/getEnv NBEAMON
/Bacc/beamOn {NBEAMON}
exit
