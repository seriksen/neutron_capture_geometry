#
# Run ganga jobs
#
# Running commands:
# hdfs dfs -mkdir -p ${base_location}/${particle}/lzap_output
# hdfs dfs -mkdir -p ${base_location}/${particle}/baccarat_verbose
# conda activate ganga
# ganga batch_submission.py
#

#particle = 'gdls_neutrons'
#particle = 'scinttank_rockgamma_th232'
#particle = 'scinttank_rockgamma_u238'
#particle = 'scinttank_rockgamma_k40'
#particle = 'scinttank_rockgamma_all'
#particle = 'cf252'
#particle = 'amli_csd1_z700mm'
#particle = 'background_neutrons_18Jun19'
#particle = 'tpc_1mev_neutrons'
#particle = 'gd_152_decay'
#particle = 'od_internals'
#particle = 'od_200kev_gamma'
#particle = 'od_200kev_alpha'
#particle = 'od_200kev_electron'
#particle = 'od_500kev_electron'
#particle = 'od_500kev_alpha'
#particle = 'od_500kev_gamma'
#particle = 'od_1000kev_electron'
#particle = 'od_1000kev_alpha'
#particle = 'od_1000kev_gamma'
#particle = 'od_2000kev_electron'
#particle = 'od_2000kev_alpha'
#particle = 'od_2000kev_gamma'
#particle = 'od_100kev_gamma'
#particle = 'amli_neutrons_only_csd1_z700mm'
#particle = 'tpc_1mev_neutrons_saved_verbose'
#particle = 'amli_neutrons_only_csd1_z700mm_saved_verbose'
#particle = 'gdls_neutrons_with_photon_evaporation'
#particle = 'background_sprectrum_gdls_internals_improved_purification'
#particle = 'background_sprectrum_gdls_internals_original_purification'
#particle = 'th228_decaychain_csd3_z700mm'
#particle = 'gdls_k40'
#particle = 'background_sprectrum_gdls_internals_original_purification_additional'
#particle = 'background_sprectrum_ocv'
#particle = 'background_spectrum_non_gdls_sr1'
#particle = 'background_spectrum_lz_components'
#particle = 'gamma_8mev'
#particle = 'gd152_decay'
#particle = 'th228_decaychain_csd3_z700mm'
#particle = 'ocs_fibre15'
#particle = 'rn219_decay'
particle = 'background_sprectrum_gdls_internals_kr85'

baccarat_version = '6.2.14'
der_version = '9.1.0'
lzap_version = '5.4.1'
base_save_location = '/user/ak18773/od_simulations/BACCARAT_{0}_DER_{1}_LZAP_{2}/OD_Spectrum'.format(baccarat_version, der_version, lzap_version)

n_jobs = 1000
start_seed = 8000
splitter_env_vals = []
for i in range(start_seed, start_seed + n_jobs):
    variable_dict = {'SEED': str(i),
                     'PARTICLE': particle,
                     'OUTPUT_FILENAME': particle + '_',
                     'MACRO': particle + '.mac',
                     'HDFS_NPY_OUTPUT_DIR': base_save_location + '/' + particle + '/baccarat_verbose/',
                     'HDFS_LZAP_OUTPUT_DIR': base_save_location + '/' + particle + '/lzap_output/',
                     'NBEAMON': '100',
                     'PYTHON_SCRIPT': 'baccarat_verbose_reader.py',
                     'THIS_BACCARAT_VERSION': baccarat_version,
                     'THIS_DER_VERSION': der_version,
                     'THIS_LZAP_VERSION': lzap_version,
                     'LZAP_STEERING_FILE': f'/cvmfs/lz.opensciencegrid.org/LZap/release-{lzap_version}/x86_64-centos7-gcc8-opt/ProductionSteeringFiles/SR1/RunLZap.py',
                     'RUN_VERBOSE_ANALYSIS': "0"
                     }

    splitter_env_vals.append(variable_dict)


    
j = Job()
j.application = Executable()
j.application.exe = File('worker_node_script.sh')
j.splitter = GenericSplitter()
j.splitter.attribute = 'application.env'
j.splitter.values = splitter_env_vals
j.inputfiles = [LocalFile('macros/{0}.mac'.format(particle))]
j.backend = Local()
j.submit()

